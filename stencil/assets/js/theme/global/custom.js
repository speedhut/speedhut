import $ from 'jquery';

module.exports = function () {
    $('li.product').each(function () {
        const onsale = $(this).find('.price--rrp');

        if (onsale.length === 1) {
            $(this).append('<div class="product-item-sale-tag">Sale</div>');
        }
    });
};

$('.header-search input, .navPages-quickSearch input').on('input', function () {
    $('.quickSearchResults').toggle(this.value.length > 0);
    $('.search-close').toggle(this.value.length > 0);
});

$('.search-close').click(function () {
    $('.quickSearchResults ul').remove();
    $('.form-input').val(null);
    $('.search-close').hide();
    $('.navPages-quickSearch').hide();
    this.hide();
});
// asdsad
$('.my-account a').click(() => {
    $('.my-account .dropdown-menu').toggle();
});

$('a.navUser-action--quickSearch').click(() => {
    $('.navPages-quickSearch').toggle();
});

$('.blog-section').load('/ .homepage-blog-posts');

$('.customize-your-gauge ul li, .actions a').click(() => {
    $('.options-overlay').toggle();
});

$('#step-one .card-item .action-btn a').click(() => {
    $('.lightbox-styles').fadeIn(300);
});

$('#step-two .card-item .action-btn a').click(() => {
    $('.lightbox-attributes').fadeIn(300);
});

$('.lightbox-styles .close').click(() => {
    $('.lightbox-styles').fadeOut(200);
});

$('.lightbox-attributes .close').click(() => {
    $('.lightbox-attributes').fadeOut(200);
});
